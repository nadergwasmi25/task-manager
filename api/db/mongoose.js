// This file will handle connection logic to the MongoDB database

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/TaskManager', { useNewUrlParser: true }).then(() => {
  console.log("Connected to mongoDB successfully!");
}).catch((e) => {
  console.log("Error connection to mongoDB ");
  console.log(e);
})



// To prevent deprectation warnings (from mongoDB native driver)
// mongoose.set('userCreateIndex', true);
// mongoose.set('useFindAndModify', false);

module.exports = {
  mongoose
}

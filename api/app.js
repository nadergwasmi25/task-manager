const express  = require('express');
const app = express();

const {mongoose} = require('./db/mongoose')

const bodyParser = require('body-parser');

// Load in the mogoose models
const { List, Task } = require('./db/models');


// Load middleware
app.use(bodyParser.json());


// ROUTES HANDLERS

// LIST ROUTES

/**
 * GET /lists
 * Pupose: Get all lists
 */
app.get('/lists', (req, res) => {
    // Return an array of all lists in the database
    List.find({}).then((lists) => {
      res.send(lists);
    })
});


/**
 * POST /lists
 * Purpose: Create a list
 */
app.post('/lists', (req, res) => {
    // Create a new list and return the new list document back to the new user (which includes the id)
    // The list information (fields) will be passed via the JSON request body
    let title = req.body.title;
    console.log(title);

    let newList = new List({
      title
    });
    newList.save().then((listDoc) => {
      // The full list document is returned (incl. id)
      res.send(listDoc);
    })
});

/**
 * PATH /lists/:id
 * Purpose: Update a specified list
 */
app.patch('/lists/:id', (req, res) => {
    // Update the specified list (list document with id in the URL) with the new values in the JSON body of the request
    List.findOneAndUpdate({ _id: req.params.id}, {
      $set: req.body
    }).then(() => {
      res.sendStatus(200);
    });

});


app.delete('/lists/:id', (req, res) => {
    // Delete the specified list (document with id in URL)
    List.findOneAndRemove({
      _id: req.params.id
    }).then((removedListDoc) => {
      res.send(removedListDoc);
    });
});


/**
 * GET /lists/:listId/tasks
 * Purpose: Get all tasks in a specific list
 */
app.get('/lists/:listId/tasks', (req, res) => {
  // Return all tasks that belong to a specific list (specified by listId)
  Task.find({
    _listId: req.params.listId
  }).then((tasks) => {
    res.send(tasks);
  });
})


/**
 * POST /lists/:listId/tasks
 * Purpose: Create a new task in a specific list
 */
app.post('/lists/:listId/tasks', (req, res) => {
  // Create a new task in a list specified by listId
  let newTask = new Task({
    title: req.body.title,
    _listId: req.params.listId
  });
  newTask.save().then((newTaskDoc) => {
    res.send(newTaskDoc);
  })
})


/**
 * PATCH /lists/:listId/tasks/:taskId
 * Purpose: Update an existing task
 */
app.patch('/lists/:listId/tasks/:taskId', (req, res) => {
  // Update an existing task (specified by taskId)
  Task.findOneAndUpdate({
    _id: req.params.taskId,
    _listId: req.params.listId
    }, {
      $set: req.body
    }
  ).then(() => {
      res.sendStatus(200);
  })
})


/**
 * DELETE /lists/:listId/tasks/:taskId
 * Purpose: Delete an existing task
 */
app.delete('/lists/:listId/tasks/:taskId', (req, res) => {
  Task.findOneAndRemove({
    _id: req.params.taskId,
    _listId: req.params.listId
  }).then((removeTaskDoc) => {
    res.send(removeTaskDoc);
  })
})



app.listen(3000, () => {
    console.log('Server in listening on port 3000');
})
